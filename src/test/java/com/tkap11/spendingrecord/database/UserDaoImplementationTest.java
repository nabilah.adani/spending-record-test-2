package com.tkap11.spendingrecord.database;

import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import com.tkap11.spendingrecord.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import javax.sql.DataSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserDaoImplementationTest {
    @Mock
    DataSource dataSource;

    @Mock
    JdbcTemplate jdbcTemplate;

    @InjectMocks
    UserDaoImplementation daoImplementation;

//    private final static ResultSetExtractor<List<User>> MULTIPLE_RS_EXTRACTOR= new ResultSetExtractor<List<User>>() {
//        @Override
//        public List<User> extractData(ResultSet rs) throws SQLException, DataAccessException {
//            return null;
//        }
//    };

    @Test
    void get() {
//        when(jdbcTemplate.query("query", MULTIPLE_RS_EXTRACTOR)).thenReturn(new ArrayList<>());
//        daoImplementation.get();
//        verify(daoImplementation, times(1)).get();
    }

    @Test
    void getByUserId() {
    }

    @Test
    void registerUser() {
    }
}