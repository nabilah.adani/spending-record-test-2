package com.tkap11.spendingrecord.service;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.Multicast;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import java.util.Set;

@Service
public class BotService {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private BotTemplate botTemplate;

    @Autowired
    private UserDatabaseService userService;

    @Autowired
    private SpendingDatabaseService spendingService;

    public Source source;

    public void greetingMessage(String replyToken) {
        registerUser(source);
        replyFlexMenu(replyToken);
    }

    private void registerUser(Source source) {
        String senderId = source.getSenderId();
        UserProfileResponse sender = getProfile(senderId);
        userService.registerUser(sender.getUserId(), sender.getDisplayName());
    }

    public void replyFlexMenu(String replyToken){
        FlexMessage flexMessage=botTemplate.createFlexMenu();
        reply(replyToken, flexMessage);
    }

    public void relpyFlexChooseCategory(String replyToken){
        FlexMessage flexMessage=botTemplate.createFlexChooseCategory();
        reply(replyToken, flexMessage);
    }
    
    public void relpyFlexSisa(String replyToken){
        FlexMessage flexMessage=botTemplate.createFlexSisa();
        reply(replyToken, flexMessage);
    }

    public void replyFlexAlarm(String replyToken){
        FlexMessage flexMessage=botTemplate.createFlexAlarm();
        reply(replyToken, flexMessage);
    }

    public void replyFlexUbah(String replyToken){
        FlexMessage flexMessage=botTemplate.createFlexUbah();
        reply(replyToken, flexMessage);
    }

    private void replyText(String replyToken, String message){
        TextMessage textMessage=new TextMessage(message);
        reply(replyToken, textMessage);
    }

    public void reply(String replyToken, Message message) {
        ReplyMessage replyMessage=new ReplyMessage(replyToken, message);
        reply(replyMessage);
    }

    private void reply(ReplyMessage replyMessage) {
        try {
            lineMessagingClient.replyMessage(replyMessage).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public UserProfileResponse getProfile(String userId){
        try {
            return lineMessagingClient.getProfile(userId).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public void push(PushMessage pushMessage){
        try {
            lineMessagingClient.pushMessage(pushMessage).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    private void pushAlarm(String to){
        TextMessage textMessage = new TextMessage("Hallo! Waktunya mencatat!")
        PushMessage pushMessage = new PushMessage(to, textMessage);
        push(pushMessage);
    }

    public void multicast(Set<String> to, Message message) {
        try {
            Multicast multicast = new Multicast(to, message);
            lineMessagingClient.multicast(multicast).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    private void sendMulticast(Set<String> sourceUsers, String txtMessage){
        TextMessage message = new TextMessage(txtMessage);
        Multicast multicast = new Multicast(sourceUsers, message);

        try {
            lineMessagingClient.multicast(multicast).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public void handleMessageEvent(MessageEvent messageEvent){
        TextMessageContent textMessageContent=(TextMessageContent) messageEvent.getMessage();
        String replyToken=messageEvent.getReplyToken();
        String userMessage=textMessageContent.getText();
        boolean userMessageIsEqualsToMenu=userMessage.equalsIgnoreCase("menu");
        String senderId = getSenderId();
        pushAlarm(senderId);
        if (userMessageIsEqualsToMenu){
            replyFlexMenu(replyToken);
        } else if (userMessage.toLowerCase().contains("catat")) {
            relpyFlexChooseCategory(replyToken);
        } else if (textMessageContent.getText().toLowerCase().contains("sisa")){
            relpyFlexSisa(replyToken);
        } else if (userMessage.toLowerCase().contains("catat")){
            relpyFlexChooseCategory(replyToken);
        } else if (userMessage.equalsIgnoreCase("register")){
//            registerUser(source);
            saveRecord(source);
            replyText(replyToken, "Proses register berhasil");
        } else if (textMessageContent.getText().toLowerCase().contains("ingatkan")){
            replyFlexAlarm(replyToken);
        } else if (textMessageContent.getText().toLowerCase().contains("ubah")){
            replyFlexUbah(replyToken);
        }
        else{
            replyText(replyToken, "Sedang dalam pengembangan");
        }

    }

    private void saveRecord(Source source) {
        String senderId = source.getSenderId();
        UserProfileResponse sender = getProfile(senderId);
        Date date = new Date();
        Timestamp ts=new Timestamp(date.getTime());
        String tm = String.valueOf(ts);
        spendingService.saveRecord(sender.getUserId(), sender.getDisplayName(), "makanan", tm, "20000");
    }
}
